'use strict';

function preventFromQuitting(error) {
    console.error(error);
    this.emit('end');
}

var gulp = require('gulp'),
    sourcemaps = require('gulp-sourcemaps'),
    less = require('gulp-less'),
    run = require('gulp-run');

var lesses = 'less/bootstrap_elements.less';


gulp.task('less', function () {
    gulp.src(lesses)
        .pipe(sourcemaps.init())
        .pipe(less())
        .on('error', preventFromQuitting)
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('css/'));
});


gulp.task('watch', function () {
    gulp.watch('**/*.less', ['less']);
});

gulp.task('bower', function () {
    run("bower install").exec('', function () {
        gulp.start('defaultDefered');
    });
});


gulp.task('default', ['bower']);
gulp.task('defaultDefered', ['watch', 'less']);
